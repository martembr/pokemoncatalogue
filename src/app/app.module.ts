import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { PokemonCardComponent } from './components/shared/pokemon-card/pokemon-card.component';
import { PokemonImageComponent } from './components/shared/pokemon-image/pokemon-image.component';
import { PokemonDetailComponent } from './components/views/pokemon-detail/pokemon-detail.component';
import { TrainerPageComponent } from './components/views/trainer-page/trainer-page.component';
import { ListAllPokemonsComponent } from './components/views/list-all-pokemons/list-all-pokemons.component';
import { StartPageComponent } from './components/views/start-page/start-page.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PokemonListComponent,
    PokemonCardComponent,
    PokemonImageComponent,
    PokemonDetailComponent,
    TrainerPageComponent,
    ListAllPokemonsComponent,
    StartPageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
