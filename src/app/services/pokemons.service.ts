import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResults } from 'src/models/apiresult.models';
import { Pokemon } from 'src/models/pokemon.model';
//import { Pokemon } from 'src/models/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class PokemonsService {

  private url:string = "https://pokeapi.co/api/v2";

  constructor(private http:HttpClient) { }

  /*
  getPokemons():Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>("https://pokeapi.co/api/v2/pokemon?limit=30");
  }*/

  getPokemons():Observable<ApiResults> {
    return this.http.get<ApiResults>("https://pokeapi.co/api/v2/pokemon?limit=30");
  }

  getPokemonByUrl(url:string):Observable<Pokemon> {
    return this.http.get<Pokemon>(url);
  }

  getPokemonById(id:string):Observable<Pokemon> {
    return this.http.get<Pokemon>(`${this.url}/pokemon/${id}/`);
  }

}
