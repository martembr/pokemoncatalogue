import { Injectable } from '@angular/core';
import { PokemonResult } from 'src/models/pokemonresult.model';
import { Trainer } from 'src/models/trainer.model';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {


  constructor() { }

  // registers the trainer in the service
  getTrainer():Trainer {
    let trainerString = localStorage.getItem('trainer');
    if (trainerString) {
      return JSON.parse(trainerString);
    }
    return null;
  }

  // checks if a user is registered as trainer
  isTrainer():boolean {
    return this.getTrainer() != null;
  }

  // checks if the trainer has a pokemon with the given id
  hasCollected(id:string):boolean {
    let trainer = this.getTrainer();
    if (trainer == null || !trainer.pokemons) {
      return false;
    }
    let pokemons = trainer.pokemons;
    return (pokemons.filter(pokemon => {
      return pokemon.id == id;
    }).length > 0);
  }

  collectPokemon(pokemonresult:PokemonResult):void {
    let trainer = this.getTrainer();
    if (trainer == null) {
      return;
    }
    // add pokemon to trainer's list of pokemons
    if (!trainer.pokemons) {
      trainer.pokemons = [ pokemonresult ];
    } else {
      trainer.pokemons.push(pokemonresult);
    }
    // update local storage
    localStorage.setItem('trainer', JSON.stringify(trainer));
  }

}
