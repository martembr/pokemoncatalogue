import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonsService } from 'src/app/services/pokemons.service';
import { PokemonResult } from 'src/models/pokemonresult.model';

@Component({
  selector: 'app-list-all-pokemons',
  templateUrl: './list-all-pokemons.component.html',
  styleUrls: ['./list-all-pokemons.component.css']
})
export class ListAllPokemonsComponent implements OnInit {

  public pokemons: PokemonResult[]; 
  public error: String = '';

  constructor(private pokemonService: PokemonsService,
              private router: Router) { }

  ngOnInit(): void {
    this.pokemonService.getPokemons().subscribe( (pokemons) => {
      this.pokemons = pokemons.results;
    } , (error) => {
      this.error = error.message;
    } );
  }

  onClick() : void {
    // redirect to trainer home
    this.router.navigateByUrl('/trainerhome'); 
  }

}
