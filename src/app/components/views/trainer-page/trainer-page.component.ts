import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonResult } from 'src/models/pokemonresult.model';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.css']
})
export class TrainerPageComponent implements OnInit {

  public pokemons:PokemonResult[];

  constructor(private router:Router) { }

  ngOnInit(): void {
    let trainer = localStorage.getItem('trainer');
    if (trainer) {
      this.pokemons = JSON.parse(trainer).pokemons;
    }
  }


  onClick() : void {
    // redirect to pokemon catalogue
    this.router.navigateByUrl('/pokemoncatalogue'); 
  }

}
