import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Trainer } from 'src/models/trainer.model';

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.css']
})
export class StartPageComponent implements OnInit {

  public trainerName: string = null;
  private trainer: Trainer = {
    name: ''
  }; 

  constructor(private router: Router) { }

  ngOnInit(): void {
    if (localStorage.getItem('trainer')) {
      this.router.navigateByUrl('/pokemoncatalogue');
    } 
  }

  onSubmit() : void {
    // register trainer in local storage
    this.trainer.name = this.trainerName;
    localStorage.setItem('trainer', JSON.stringify(this.trainer));
    // redirect to catalogue
    this.router.navigateByUrl('/pokemoncatalogue'); 
  }

}
