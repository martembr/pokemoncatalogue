import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonsService } from 'src/app/services/pokemons.service';
import { TrainerService } from 'src/app/services/trainer.service';
import { Pokemon } from 'src/models/pokemon.model';
import { PokemonResult } from 'src/models/pokemonresult.model';
import { Trainer } from 'src/models/trainer.model';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.css']
})
export class PokemonDetailComponent implements OnInit {

  public id:string = '1';
  public pokemon: Pokemon = null;
  public error:string = '';
  public trainer: boolean = false;
  public hasCollected: boolean = false;

  constructor(private pokemonService: PokemonsService,
              private route: ActivatedRoute,
              private trainerService: TrainerService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.pokemonService.getPokemonById(this.id).subscribe( (pokemon) => {
      this.pokemon = pokemon;
    } , (error) => {
      this.error = error.message;
    } );
    this.trainer = this.trainerService.isTrainer();
    this.hasCollected = this.trainerService.hasCollected(this.id);
  }

  collectPokemon():void {
    // create PokemonResult object
    let resulturl = `https://pokeapi.co/api/v2/pokemon/${this.pokemon.id}/`;
    let pokemonresult:PokemonResult = {
      name: this.pokemon.name,
      url: resulturl,
      id: this.id
    }
    this.trainerService.collectPokemon(pokemonresult);
    // update hasCollected
    this.hasCollected = true;
    
  }

}
