import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonResult } from 'src/models/pokemonresult.model';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.css']
})
export class PokemonCardComponent implements OnInit {

  @Input() pokemon:PokemonResult;  

  constructor(private router: Router) { }

  ngOnInit(): void {
    if (!this.pokemon.id) {
      let wordlist = this.pokemon.url.split("/");
      this.pokemon.id = wordlist[wordlist.length-2];
    }
    this.pokemon.imageurl = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${this.pokemon.id}.png`;
  }

  onCardClicked() :void {
    this.router.navigateByUrl(`/detail/${this.pokemon.id}`);
  }

}
