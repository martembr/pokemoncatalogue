import { Component, Input, OnInit } from '@angular/core';
import { PokemonResult } from 'src/models/pokemonresult.model';

@Component({
  selector: 'app-pokemon-image',
  templateUrl: './pokemon-image.component.html',
  styleUrls: ['./pokemon-image.component.css']
})
export class PokemonImageComponent implements OnInit {

  @Input() pokemon: PokemonResult;


  constructor() { }

  ngOnInit(): void {
  }

}
