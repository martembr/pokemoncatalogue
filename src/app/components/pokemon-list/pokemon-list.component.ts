import { Component, Input, OnInit } from '@angular/core';
//import { PokemonsService } from 'src/app/services/pokemons.service';
import { PokemonResult } from 'src/models/pokemonresult.model';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {

  @Input() pokemons:PokemonResult[]; 
  //public pokemons: Pokemon[]; 
  //public pokemons: PokemonResult[]; 

  //public error: String = '';

  //constructor(private pokemonService: PokemonsService) { }
  constructor() { }

  ngOnInit(): void {
    /*this.pokemonService.getPokemons().subscribe( (pokemons) => {
      this.pokemons = pokemons.results;
    } , (error) => {
      this.error = error.message;
    } );*/
  }

}
