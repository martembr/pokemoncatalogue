import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListAllPokemonsComponent } from './components/views/list-all-pokemons/list-all-pokemons.component';
import { PokemonDetailComponent } from './components/views/pokemon-detail/pokemon-detail.component';
import { StartPageComponent } from './components/views/start-page/start-page.component';
import { TrainerPageComponent } from './components/views/trainer-page/trainer-page.component';
import { AuthGuard } from './guards/auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: StartPageComponent
  },
  {
    path: 'trainerhome',
    component: TrainerPageComponent
  },
  {
    path: 'detail/:id',
    component: PokemonDetailComponent
  },
  {
    path: 'pokemoncatalogue',
    component: ListAllPokemonsComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
