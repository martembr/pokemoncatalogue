import { PokemonResult } from './pokemonresult.model';

export interface Trainer {
    name: string,
    pokemons?: PokemonResult[]
}