export interface PokemonResult {
    name: string;
    url: string;
    id?: string;
    imageurl?: string;
}