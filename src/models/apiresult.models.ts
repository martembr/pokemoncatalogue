
export interface ApiResults {
    count?: string;
    next?: string;
    previous?: any;
    results: any[];
}