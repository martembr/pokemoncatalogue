import { Pokemon } from 'src/models/pokemon.model';

export const MOCK_POKEMONS: Pokemon[] = [
    {
        id: 1,
        name: "Pokemon1"
    },
    {
        id: 2,
        name: "Pokemon2"
    },
    {
        id: 3,
        name: "Pokemon3"
    },
    {
        id: 4,
        name: "Pokemon4"
    }
];